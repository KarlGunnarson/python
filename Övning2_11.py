#Program to calculate miles & gas consumption
CurrentMiles=int(input("Mätarställning idag? "))
MilesYearAgo=int(input("Mätarställning för ett år sedan? "))
RunningMiles=CurrentMiles-MilesYearAgo
print("Antal körda mil:   ",RunningMiles)
Consumption=float(input("Antal liter bensin: "))

ConsumptionOneMile=Consumption/RunningMiles
print(f'Förbrukning per mil {ConsumptionOneMile:0.2f}')