# skriv ett program som skapar en lista med 100 slumpmässiga heltal i intervallet 1 till 1000.
# Programmet ska sedan skruva ut det minsta och det största av talen samt beräkna och skruva ut slumptalen medelvärde
import random
lista=[]
for i in range(1,101):
    lista+=[random.randint(1,1000)]


print('minsta talet är ',min(lista))
print('Högsta talet är ',max(lista))
print('Medelvärdet är ',sum(lista)/100)

