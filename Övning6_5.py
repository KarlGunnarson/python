# Kontantkort, lägsta pris
namn = []
pris = []
while True:
    s = input('Namn och pris för ett kort: ')
    if s == '':
        break
    i = (s.rpartition(' ')) 
    x=len(i)                    # sök index före priset
    namn.append(i[0])           # bilda skiva med namnet
    pris.append(float(i[x-1]))  # bilda skiva med priset
m = min(pris)                   # sök lägsta pris
k = pris.index(m)               # index för lägsta pris
print(namn[k] + ' är billigast')
print(f'Kostnad: {m:1.2f} kr/månad')