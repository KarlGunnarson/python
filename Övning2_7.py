import math
fRadie=float(input('Cirklens radie '))
fDiameter=fRadie*2

#Calc diameter of the circle & print 
#formated string with 3 decimals
print(f'Omrkets = {math.pi*fDiameter:.3f}')

#Calc Area of the circle & print 
#formated string with 3 decimals
print(f'Area = {math.pi*math.pow(fRadie,2):.3f}')
