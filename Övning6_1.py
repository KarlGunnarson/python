#Geometrisk talföjd
import math
iRange=int(input('Hur många tal vill ha i en geometrisk talföjd? '))
k       =3
iList   =[0]*iRange
iList[0]=2
iCounter=1
for i in range(1,iRange):
    iList[i]=iList[0]*(k**iCounter)
    iCounter+=1
print(iList)
